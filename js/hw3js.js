// функции это набор различных действий, которые мы описываем один раз и потом можем вызать везде по ходу кода
// аргумент передается функции чтобы использовать функцию повторно с иными значениями


let userFirstNumber = prompt("Enter Your First number: "); //нецелые вводяться через точку. 0.3 например
while (userFirstNumber === "") {
    userFirstNumber = prompt('Enter the first number correct please: ', `${userFirstNumber}`);
}
userFirstNumber = Number(userFirstNumber);
while (Number.isNaN(userFirstNumber)) {
    userFirstNumber = +prompt('Enter the first number correct please: ', `${userFirstNumber}`);
}


let userSecondNumber = prompt("Enter Your Second number:");
while (Number.isNaN(userSecondNumber) || userSecondNumber === "") {
    userSecondNumber = prompt('Enter the second number correct please: ', `${userSecondNumber}`);
}
userSecondNumber = Number(userSecondNumber);
while (Number.isNaN(userSecondNumber)) {
    userSecondNumber = +prompt('Enter the second number correct please: ', `${userSecondNumber}`);
}


let userOperationChoice = prompt("Please enter the wanted operation as symbol(+,-,*,/)");

let pattern = new RegExp(/[~`!#$%\^&=';{}|":<>\?]/);
let numericPattern = new RegExp (/[0-9]/);
let alphabeticPattern = new RegExp (/[a-z]/);
while (pattern.test(userOperationChoice) || numericPattern.test(userOperationChoice) || alphabeticPattern.test(userOperationChoice)) {
        userOperationChoice = prompt("Please enter a wanted operation correct as symbol(+,-,*,/)");
}

function myCalculator(value1, value2, operator) {
    switch (operator) {
        case "+":
            return value1+value2;
            break;
        case "-":
            return value1-value2;
            break;
        case "*":
            return value1*value2;
        case "/":
            if (value2 !== 0) {
            return value1/value2;
            } else {
                return "Error!(Number 0 is not allowed!)";
            }

    }
}


let result = myCalculator(userFirstNumber, userSecondNumber, userOperationChoice);
console.log(result);
//как предусмотреть случай с числами меньше 0?
// например 0.1 + 0.2 даст тонну знаков после запятой
//метод toFixed не решает данный случай так как если мы округлим результат например до 5 знаков
//то всегда найдутся числа с 6 и больше знаками после запятоой(напрмер 0.156352 + 0.3235325) и тогда уже не будет
//верно считать сумму